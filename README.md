# CMake Cyclic Dependency Check
One remaining reason to retain GNUmake builds for Geant4 is cycle detection.
CMake will however detect direct or indirect cycles between shared libraries directly.
To demonstrate, this mini-project shows some simple cases:

# Indirect cycles
A common case for cycles creeping in to the linking tree is indirectly, e.g.
"C links to B", "B links to A", "A links to C". To demonstrate this, try configuring the
project with:

```
$ mkdir build
$ cd build
$ cmake ..
```

This will suceed as despite the libraries `foo-hidden`, `bar-hidden`, and `baz-hidden` linking to each
other in a cycle. CMake allows cycles for the (default) `STATIC` library type. It takes care of this
by multi-linking (e.g. `A B A B`). Now try configuring with

```
$ cmake -DBUILD_SHARED_LIBS=ON ..
```

and it should now error out with:

```
-- Configuring done
CMake Error: The inter-target dependency graph contains the following strongly connected component (cycle):
  "baz-hidden" of type SHARED_LIBRARY
    depends on "bar-hidden" (weak)
  "bar-hidden" of type SHARED_LIBRARY
    depends on "foo-hidden" (weak)
  "foo-hidden" of type SHARED_LIBRARY
    depends on "baz-hidden" (weak)
At least one of these targets is not a STATIC_LIBRARY.  Cyclic dependencies are allowed only among static libraries.
CMake Generate step failed.  Build files cannot be regenerated correctly.
```

Whilst cycles are picked up, only the last one in the build will be reported. If you look at
the `CMakeLists.txt` file, the above cycle is the last one, with the earlier `foo-direct` and
`bar-direct` also having a cycle (See below on how to show that CMake can also handle this type
of cycle).


# Direct cycles
To see the case for direct cycles, checkout tag `v0.1.0`

The simplest case of "A links to B" *and* "B links to A". Try configuring this project
with:

```
$ mkdir build
$ cd build
$ cmake ..
```

This will suceed as despite the libraries `foo-direct` and `bar-direct` linking to each
other, CMake allows cycles for the (default) `STATIC` library type. It takes care of this
by multi-linking (e.g. `A B A B`). Now try configuring with

```
$ cmake -DBUILD_SHARED_LIBS=ON ..
```

and it should now error out with:

```
-- Configuring done
CMake Error: The inter-target dependency graph contains the following strongly connected component (cycle):
  "bar-direct" of type SHARED_LIBRARY
    depends on "foo-direct" (weak)
  "foo-direct" of type SHARED_LIBRARY
    depends on "bar-direct" (weak)
At least one of these targets is not a STATIC_LIBRARY.  Cyclic dependencies are allowed only among static libraries.
CMake Generate step failed.  Build files cannot be regenerated correctly.
```
